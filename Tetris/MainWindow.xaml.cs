﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Tetris
{
    public partial class MainWindow : Window
    {
        
        
        private DispatcherTimer timer;
        private GameBoard gameBoard;
        public MainWindow()
        {
            InitializeComponent();
            this.KeyDown += new KeyEventHandler(OnKeyDown);
            timer = new DispatcherTimer();
            timer.Tick += new EventHandler(GameTick);
            timer.Interval = new TimeSpan(0, 0, 0, 0, 600);
            GameStart();
        }

        private void GameStart()
        {
            tetrisGrid.Children.Clear();
            gameBoard = new GameBoard(tetrisGrid);
            txtGameOver.Foreground = Brushes.Transparent;
            timer.Start();
        }
        public void GameTick(object sender, EventArgs e)
        {
            txtScore.Text = gameBoard.Score.ToString();
            txtLines.Text = gameBoard.Lines.ToString();
            gameBoard.MoveBlockDown();
            CheckState();

        }

        public void CheckState()
        {
            if (gameBoard.CanPlay)
            {
                PauseGame();
                txtGameOver.Foreground = Brushes.Red;

            }
        }
        public void OnKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Left: if (timer.IsEnabled) { gameBoard.MoveBlockLeft(); } break;
                case Key.Right: if (timer.IsEnabled) { gameBoard.MoveBlockRight(); } break;
                case Key.Down: if (timer.IsEnabled) { gameBoard.MoveBlockDown(); } break;
                case Key.Up: if (timer.IsEnabled) { gameBoard.RotateBlock(); } break;

                case Key.P: PauseGame(); break;
                case Key.N: GameStart(); break;

                default: break;
            }
        }

        private void PauseGame()
        {
            if (timer.IsEnabled) { timer.Stop(); }
            else { timer.Start(); }
        }
        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            GameStart();
        }
    }
}
