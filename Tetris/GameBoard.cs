﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Tetris
{
    class GameBoard
    {
        private int rows;
        private int cols;

        public int Score { set; get; }
        public int Lines { set; get; }

        private TBlock actualBlock;
        private Label[,] BlockControls;

        public GameBoard(Grid gameGrid)
        {
            rows = gameGrid.RowDefinitions.Count;
            cols = gameGrid.ColumnDefinitions.Count;

            Lines = 0;
            Score = 0;

            BlockControls = new Label[cols, rows];

            for (var i = 0; i < cols; i++)
            {
                for (var j = 0; j < rows; j++)
                {
                    BlockControls[i, j] = new Label();
                    BlockControls[i, j].Background = Brushes.Transparent;
                    BlockControls[i, j].BorderBrush = Brushes.LightGray;
                    BlockControls[i, j].BorderThickness = new Thickness(0.3);
                    Grid.SetRow(BlockControls[i, j], j);
                    Grid.SetColumn(BlockControls[i, j], i);
                    gameGrid.Children.Add(BlockControls[i, j]);
                }
            }

            actualBlock = new TBlock();
            DrawBlock();
        }

        private void DrawBlock()
        {
            Point position = actualBlock.GetPosition();
            Point[] shape = actualBlock.Shape;
            Brush color = actualBlock.Color;

            foreach (Point S in shape)
            {
                BlockControls[(int)(S.X + position.X) + ((cols / 2) - 1), (int)(S.Y + position.Y) + 2].Background = color;
            }
        }

        public bool CanPlay { get; private set; } = false;

        private void eraseBlock()
        {
            Point position = actualBlock.GetPosition();
            Point[] shape = actualBlock.Shape;

            foreach (Point S in shape)
            {
                BlockControls[(int)(S.X + position.X) + ((cols / 2) - 1),
                              (int)(S.Y + position.Y) + 2].Background = Brushes.Transparent;
            }
        }

        private void CheckRows()
        {
            bool full;

            for (var i = rows - 1; i > 0; i--)
            {
                full = true;
                for (var j = 0; j < cols; j++)
                {
                    if (BlockControls[j, i].Background == Brushes.Transparent)
                    {
                        full = false;
                    }
                }

                if (full)
                {
                    RemoveRow(i);
                    Score += 100;
                    Lines += 1;
                }
            }
        }

        private void RemoveRow(int row)
        {
            for (var i = row; i > 0; i--)
            {
                for (var j = 0; j < cols; j++)
                {
                    BlockControls[j, i].Background = BlockControls[j, i - 1].Background;
                }
            }
        }

        public void MoveBlockLeft()
        {
            Point position = actualBlock.GetPosition();
            Point[] shape = actualBlock.Shape;
            bool move = true;

            eraseBlock();

            foreach (Point S in shape)
            {
                if (((int)(S.X + position.X) + ((cols / 2) - 1) - 1) < 0) { move = false; }

                else if (BlockControls[((int)(S.X + position.X) + ((cols / 2) - 1) - 1),
                    (int)(S.Y + position.Y) + 2].Background != Brushes.Transparent)
                {
                    move = false;
                }
            }

            if (move)
            {
                actualBlock.MoveLeft();
                DrawBlock();
            }

            else
            {
                DrawBlock();
            }
        }

        public void MoveBlockRight()
        {
            Point position = actualBlock.GetPosition();
            Point[] shape = actualBlock.Shape;
            bool move = true;

            eraseBlock();

            foreach (Point S in shape)
            {
                if (((int)(S.X + position.X) + ((cols / 2) - 1) + 1) >= cols)
                {
                    move = false;
                }

                else if (BlockControls[((int)(S.X + position.X) + ((cols / 2) - 1) + 1),
                    (int)(S.Y + position.Y) + 2].Background != Brushes.Transparent)
                {
                    move = false;
                }
            }

            if (move)
            {
                actualBlock.MoveRight();
                DrawBlock();
            }

            else
            {
                DrawBlock();
            }
        }

        public void MoveBlockDown()
        {
            Point position = actualBlock.GetPosition();
            Point[] shape = actualBlock.Shape;
            bool move = true;

            eraseBlock();

            foreach (Point S in shape)
            {
                if (((int)(S.Y + position.Y) + 3) >= rows)
                {
                    move = false;
                }

                else if (BlockControls[((int)(S.X + position.X) + ((cols / 2) - 1)),
                    (int)(S.Y + position.Y) + 3].Background != Brushes.Transparent)
                {
                    move = false;
                }
            }

            if (move)
            {
                actualBlock.MoveDown();
                DrawBlock();
            }

            else
            {
                DrawBlock();
                CheckRows();

                if ((int)position.Y == 0)
                {
                    CanPlay = true;
                    DrawBlock();
                }

                else
                {
                    actualBlock = new TBlock();
                }

            }
        }

        public void RotateBlock()
        {
            Point position = actualBlock.GetPosition();
            Point[] shape = actualBlock.Shape;
            Point[] S = new Point[4];

            bool move = true;
            shape.CopyTo(S, 0);
            eraseBlock();

            for (var i = 0; i < S.Length; i++)
            {
                double x = S[i].X;
                S[i].X = S[i].Y * -1;
                S[i].Y = x;

                if (((int)((S[i].Y + position.Y) + 2)) >= rows)
                {
                    move = false;
                }

                else if (((int)(S[i].X + position.X) + ((cols / 2) - 1)) < 0)
                {
                    move = false;
                }

                else if (((int)(S[i].X + position.X) + ((cols / 2) - 1)) >= rows)
                {
                    move = false;
                }

                else if (BlockControls[((int)(S[i].X + position.X) + ((cols / 2) - 1)),
                                       (int)(S[i].Y + position.Y) + 2].Background != Brushes.Transparent)
                {
                    move = false;
                }
            }

            if (move)
            {
                actualBlock.Rotate();
                DrawBlock();
            }

            else
            {
                DrawBlock();
            }
        }
    }
}
