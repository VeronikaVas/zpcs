﻿using System;
using System.Windows;
using System.Windows.Media;

namespace Tetris
{
    class TBlock
    {
        public Point position;
        public Point[] Shape { set; get; }
        public Brush Color { set; get; }
        private bool rotate;

        public TBlock()
        {
            position = new Point(0, 0);
            Color = Brushes.Transparent;
            Shape = RandomShape();
        }

        public Point GetPosition() => position;

        private Point[] RandomShape()
        {
            Random rand = new Random();

            switch (rand.Next() % 7)
            {

                case 0: // I
                    rotate = true;
                    Color = GetColor("#EF5350");
                    return new Point[]
                        {
                        new Point(0,0),
                        new Point(-1,0),
                        new Point(1,0),
                        new Point(2,0)
                        };

                case 1: //J
                    rotate = true;
                    Color = GetColor("#AB47BC");
                    return new Point[]
                        {
                        new Point(1,-1),
                        new Point(-1,0),
                        new Point(0,0),
                        new Point(1,0)
                        };

                case 2: //L
                    rotate = true;
                    Color = GetColor("#26C6DA");
                    return new Point[]
                        {
                        new Point(0,0),
                        new Point(-1,0),
                        new Point(1,0),
                        new Point(0,-1)
                        };

                case 3: // o
                    rotate = false;
                    Color = GetColor("#FFCA28");
                    return new Point[]
                        {
                        new Point(0,0),
                        new Point(0,1),
                        new Point(1,0),
                        new Point(1,1)
                        };

                case 4: // S
                    rotate = true;
                    Color = GetColor("#9CCC65");
                    return new Point[]
                        {
                        new Point(0,0),
                        new Point(-1,0),
                        new Point(0,-1),
                        new Point(1,0)
                        };

                case 5: // T
                    rotate = true;
                    Color = GetColor("#7E57C2");
                    return new Point[]
                        {
                        new Point(0,0),
                        new Point(-1,0),
                        new Point(0,-1),
                        new Point(1,0)
                        };

                case 6: //z
                    rotate = true;
                    Color = GetColor("#EC407A");
                    return new Point[]
                        {
                        new Point(0,0),
                        new Point(-1,0),
                        new Point(0,1),
                        new Point(1,1)
                        };
            }
            return null;
        }
        public SolidColorBrush GetColor(string hex) => (SolidColorBrush)(new BrushConverter().ConvertFrom(hex));

        public void MoveLeft() => position.X -= 1;

        public void MoveRight() => position.X += 1;

        public void MoveDown() => position.Y += 1;

        public void Rotate()
        {
            if (rotate)
            {
                for (var i = 0; i < Shape.Length; i++)
                {
                    double x = Shape[i].X;
                    Shape[i].X = Shape[i].Y * -1;
                    Shape[i].Y = x;
                }
            }
        }
    }
}
