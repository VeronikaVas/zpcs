﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_plat_konzultanta
{
    class Program
    {
        static void Main(string[] args)
        {
            double dailyRate = nactiDouble("Zadej denní sazbu: ");
            int noOfDays = nactiInt("Zadej počet dnů: ");
            zapisHonorar(spocitejHonorar(dailyRate, noOfDays));

            zapisHonorar(spocitejHonorar());
            zapisHonorar(spocitejHonorar(dailyRate: 100));
            zapisHonorar((int)10);


        }

        public static double nactiDouble(string p)
        {
            Console.Write(p);
            string line = Console.ReadLine();
            return double.Parse(line);
        }

        public static int nactiInt(string d)
        {
            Console.Write(d);
            string line = Console.ReadLine();
            return int.Parse(line);
        }

        public static double spocitejHonorar(double dailyRate = 1000, int noOfDays = 1)
        {
            return dailyRate * noOfDays;
        }

        public static void zapisHonorar(double result)
        {
            Console.WriteLine("Plat konzultanta je: {0}", result * 1.1);
        }

        public static void zapisHonorar(int result)
        {
            Console.WriteLine("Plat konzultanta je: {0}", result * 1.1);
        }
    }
}
