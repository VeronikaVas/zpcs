﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace _7_kreslici_platno
{
    class Square : IShape, IDrawing
    {
        private double side;
        private int x = 0, y = 0;

        private Rectangle square;

        public Square(double s)
        {
            this.side = s;
        }

        public double Area()
        {
            return 4 * side;
        }

        public double Perimeter()
        {
            return side * side;
        }

        public void SetCoordinates(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public override string ToString()
        {
            return "Čtverec (" + x.ToString() + "," + y.ToString() + "), obsah:" + Area().ToString() + ", obvod:" + Perimeter().ToString();
        }

        public void Draw(Canvas c)
        {
            if (this.square != null)
            {
                c.Children.Remove(this.square);
            }
            else
            {
                this.square = new Rectangle();
            }
            this.square.Height = this.side;
            this.square.Width = this.side;
            Canvas.SetTop(this.square, this.y);
            Canvas.SetLeft(this.square, this.x);
            c.Children.Add(square);
        }

        public void SetColor(Color barva)
        {
            if(square != null)
            {
                SolidColorBrush stetec = new SolidColorBrush(barva);
                square.Fill = stetec;
            }
        }

    }
}
