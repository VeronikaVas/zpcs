﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace _7_kreslici_platno
{
    class Circle : IShape, IDrawing
    {
        private double r;
        private int x = 0, y = 0;

        private Ellipse circle;

        public Circle(double r)
        {
            this.r = r;
        }
        public double Area()
        {
            return Math.PI * r * r;
        }

        public void Draw(Canvas c)
        {
            if (this.circle != null)
            {
                c.Children.Remove(this.circle);
            }
            else
            {
                this.circle = new Ellipse();
            }
            this.circle.Height = this.r;
            this.circle.Width = this.r;
            Canvas.SetTop(this.circle, this.y);
            Canvas.SetLeft(this.circle, this.x);
            c.Children.Add(circle);
        }

        public double Perimeter()
        {
            return 2 * Math.PI * r;
        }

        public void SetColor(Color barva)
        {
            if (circle != null)
            {
                SolidColorBrush stetec = new SolidColorBrush(barva);
                circle.Fill = stetec;
            }
        }

        public void SetCoordinates(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public override string ToString()
        {
            return "Kruh (" + x.ToString() + "," + y.ToString() + "), obsah:" + Area().ToString() + ", obvod:" + Perimeter().ToString();
        }
    }
}
