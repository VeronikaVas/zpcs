﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7_kreslici_platno
{
    interface IShape
    {
        double Area();
        double Perimeter();
        void SetCoordinates(int x, int y);

    }
}
