﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _7_kreslici_platno
{
    public partial class MainWindow : Window
    {
        private ArrayList shapes;
        private Random rNum;
        public MainWindow()
        {
            InitializeComponent();
            shapes = new ArrayList();
            rNum = new Random();
        }

        private void canvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Point mys = e.GetPosition(canvas);
            Square s = new Square(rNum.Next(50, 100));
            s.SetCoordinates((int)mys.X, (int)mys.Y);
            shapes.Add(s);
            UpdateShapeList(shapes);

            if (s is IDrawing)
            {
                IDrawing drawingSquare = s;
                drawingSquare.Draw(canvas);
                drawingSquare.SetColor(Colors.BlueViolet);
            }
        }

        private void canvas_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            Point mys = e.GetPosition(canvas);
            Circle c = new Circle(rNum.Next(50, 100));
            c.SetCoordinates((int)mys.X, (int)mys.Y);
            

            if (c is IDrawing)
            {
                IDrawing drawingCircle = c;
                drawingCircle.Draw(canvas);
                drawingCircle.SetColor(Colors.Orchid);
            }

            shapes.Add(c);
            UpdateShapeList(shapes);


        }

        private void UpdateShapeList(ArrayList seznam)
        {
            txtList.Text = "";
            foreach (object o in seznam)
            {
                if (o is IShape) txtList.Text += o.ToString() + "\n";
            }
        }

    }
}
