﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls; 
using System.Windows.Media;

namespace _7_kreslici_platno
{
    interface IDrawing
    {
        void SetColor(Color barva);
        void Draw(Canvas platno);
    }
}
