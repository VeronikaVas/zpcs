﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _2_obvod_trojuhelniku
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        protected bool TriangleExistence(double a, double b, double c)
        {
            if (a + b < c) return false;
            if (a + c < b) return false;
            if (b + c < a) return false;
            return true;
        }

        protected double TrianglePerimeter(double a, double b, double c)
        {
            return a + b + c;
        }

        private void buttonCount_Click(object sender, RoutedEventArgs e)
        {
            double a = Convert.ToInt32(tA.Text);
            double b = Convert.ToInt32(tB.Text);
            double c = Convert.ToInt32(tC.Text);
            bool ok = this.TriangleExistence(a, b, c);

            if (ok)
            {
                labelResult.Content = "Trojúhelník lze sestrojit\n";
                labelResult.Content += "Obvod: " + this.TrianglePerimeter(a, b, c).ToString();
            }
            else
            {
                labelResult.Content = "Trojúhelník nelze sestrojit";
            }
        }
    }
}
