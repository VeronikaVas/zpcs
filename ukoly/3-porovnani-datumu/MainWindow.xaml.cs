﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _3_porovnani_datumu
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnCompare_Click(object sender, RoutedEventArgs e)
        {
         
            int rozdil = CompareDates(dateFirst.SelectedDate.Value, dateSecond.SelectedDate.Value);
            Print(rozdil);
        }

        private int CompareDates(DateTime left, DateTime right)
        {
            return DateTime.Compare(left, right);
        }

        private void Print(int rozdil)
        {
            if (rozdil == 0) lblResult.Content = "Obě data jsou stejná.";
            if (rozdil == 1) lblResult.Content = "První datum je větší než druhé.";
            if (rozdil == -1) lblResult.Content = "Druhé datum je větší než první.";
        }

        private void btnEnd_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
