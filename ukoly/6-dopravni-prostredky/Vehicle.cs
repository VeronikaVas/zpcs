﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6_dopravni_prostredky
{
    class Vehicle
    {
        protected int number;
        protected string activity;
        protected int nWheels;
        private static int count = 0;

        protected int price = 100;

        public Vehicle()
        {
            number += count;
        }

        public Vehicle(int wheels)
        {
            this.nWheels = wheels;
        }


        public override string ToString()
        {
            return "Vozidlo číslo:" + number.ToString() + ", činnost:" + activity + ", počet kol:" +
            nWheels.ToString() + ".";
        }
        public double RentalPrice(int hours)
        {
            return hours * price;
        }
    }
}
