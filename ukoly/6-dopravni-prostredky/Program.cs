﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6_dopravni_prostredky
{
    class Program
    {
        static void Main(string[] args)
        {
            Vehicle v1 = new Vehicle();
            Console.WriteLine(v1.ToString());
            Car a1 = new Car();
            Console.WriteLine(a1.ToString());
            Airplane l1 = new Airplane();
            Console.WriteLine(l1.ToString());

            Vehicle v2 = a1;
            Console.WriteLine(v2.ToString());
            Car a2 = v2 as Car;
            Console.WriteLine(a2.ToString());
            //Airplane l2 = v2 as Airplane;
            //Console.WriteLine(l2.ToString());

        }
    }
}
