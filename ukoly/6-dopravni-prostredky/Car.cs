﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6_dopravni_prostredky
{
    class Car : Vehicle
    {
        private static int count = 0;

        public Car()
        {
            this.number = ++count;
            this.activity = "jezdí";
            this.nWheels = 4;
        }

        public Car(int wheels) : base(wheels)
        {
            this.number = ++count;
            this.activity = "jezdí";
        }

        public override string ToString()
        {
            return "Auto číslo:" + number.ToString() + ", činnost:" + activity + ", počet kol: " + nWheels.ToString() + ".";
        }

    }
}
