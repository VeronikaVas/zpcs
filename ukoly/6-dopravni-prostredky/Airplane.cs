﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6_dopravni_prostredky
{
    class Airplane : Vehicle
    {
        private static int count = 0;

        public Airplane()
        {
            this.number = ++count;
            this.activity = "létá";
            this.nWheels = 3;
        }

        public Airplane(int wheels) : base(wheels)
        {
            this.number = ++count;
            this.activity = "létá";
        }


        public override string ToString()
        {
            return "Letadlo číslo:" + number.ToString() + ", činnost:" + activity + ", počet kol: " + nWheels.ToString() + ".";
        }

        //překrytí bázové metody --> nebo použít virtual v bázové
        new public double RentalPrice(int hours)
        {
            return hours * price * 1.5;
        }

    }
}
