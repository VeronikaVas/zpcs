﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8_osoba
{
    class PersonsList
    {
        private ArrayList persons = new ArrayList();

        public void Add(Person p)
        {
            persons.Add(p);
        }

        public Person this[string s]
        {
            get
            {
                foreach (Person p in persons)
                {
                    if ((p.Name + " " + p.LastName) == s)
                        return p;
                }
                return null;
            }
        }

        public override string ToString()
        {
            string s = "";
            foreach (Person p in persons)
                s += p + "\n";
            return s;
        }
    }
}
