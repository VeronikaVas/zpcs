﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8_osoba
{
    class Person
    {
        public string Name { set; get; }
        public string LastName { set; get; }
        public string Address { set; get; }
        public int Age { set; get; }
        public Person(string name, string lastn, string address, int age)
        {
            this.Name = name;
            this.LastName = lastn;
            this.Address = address;
            this.Age = age;
        }
        public override string ToString()
        {
            return Name + " " + LastName + " : " + Address + ", " + Age;
        }

    }
}
