﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _5_tiketovaci_system
{

    public partial class MainWindow : Window
    {
        private Queue customers;
        private int n = 1;
        public MainWindow()
        {
            InitializeComponent();
            customers = new Queue();

        }

        private void btnCustomer_Click(object sender, RoutedEventArgs e)
        {
            customers.Enqueue(n++);
            UpdateQueue();
        }

        private void btnService_Click(object sender, RoutedEventArgs e)
        {
            if (customers.Count > 0)
            {
                lblActual.Content = customers.Dequeue().ToString();
                UpdateQueue();
            }
        }

        private void UpdateQueue()
        {
            lblWaiting.Content = "";
            foreach (int c in customers)
            {
                lblWaiting.Content += c.ToString() + "\n";
            }

        }
    }
}
