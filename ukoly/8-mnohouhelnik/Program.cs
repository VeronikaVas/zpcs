﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8_mnohouhelnik
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Polygon square = new Polygon();
                Polygon triangle = new Polygon { Sides = 3 };
                Polygon pentagon = new Polygon { Length = 15.5, Sides = 5};

                Console.WriteLine("Ctverec: pocet stran je {0}, delka stran je {1}, obsah je {2:F2}.", square.Sides, square.Length, square.Area());
                Console.WriteLine("Trojuhelnik: pocet stran je {0}, delka stran je {1}, obsah je {2:F2}.", triangle.Sides, triangle.Length, triangle.Area());

                Console.WriteLine("Pětiuhelnik: pocet stran je {0}, delka stran je {1}, obsah je {2:F2}.", pentagon.Sides, pentagon.Length, pentagon.Area());

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
