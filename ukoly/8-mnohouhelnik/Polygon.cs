﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8_mnohouhelnik
{
    class Polygon
    {     
        public int Sides { get; set; }
        public double Length { get; set; }

        public Polygon()
        {
            this.Sides = 4;
            this.Length = 10.0;
        }

        public double Area()
        {
            return (double)(Sides * Math.Pow((double)Length, 2)) / 4 * (1 / Math.Tan(Math.PI / Sides));
        }

    }
}
