﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _3_prvocisla
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnCount_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int n = int.Parse(txtbInput.Text);
                if (n <= 0)
                    throw new NotNaturalNumberException("Zadejte prosím celé kladné číslo");
                txtbPrimeNumbers.Text = "";
                for (var i = 2; i < n; i++)
                {
                    if (IsPrimeNumber(i))
                        txtbPrimeNumbers.Text += i + ", ";
                }
            }
            catch (NotNaturalNumberException ex)
            {
                txtbPrimeNumbers.Text = "";
                MessageBox.Show(ex.Message);
            }
            catch (OverflowException)
            {
                txtbPrimeNumbers.Text = "";
                MessageBox.Show("Zadali jste moc vysoké číslo");
            }
            catch (Exception)
            {
                txtbPrimeNumbers.Text = "";
                MessageBox.Show("Prosím zadejte číslo");
            }
        }

        protected bool IsPrimeNumber(int x)
        {
            for (var i = 2; i < x; i++)
            {
                if (x % i == 0)
                    return false;
            }
            return true;
        }
    }

    public class NotNaturalNumberException : Exception
    {
        public NotNaturalNumberException(string message) : base(message) { }
    }

}
