﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5_karetni_hra
{
    class Player
    {
        public int max;
        private int count = 0;

        private PlayingCard[] cards;


        public Player()
        {
            this.max = 8;
            cards = new PlayingCard[this.max];
        }

        public Player(int m)
        {
            this.max = m;
            cards = new PlayingCard[this.max];
        }
        public override string ToString()
        {
            string s = "";
            foreach (PlayingCard c in this.cards)
            {
                if (c != null) s += c.ToString() + Environment.NewLine;
            }

            return s;
        }

        public void AddCard(PlayingCard card)
        {
            if (this.count >= max)
            {
                throw new ArgumentException("Příliš mnoho karet.");
            }
            this.cards[this.count] = card;
            this.count++;
        }
    }
}
