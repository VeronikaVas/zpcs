﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5_karetni_hra
{
    enum Colors { Kříže, Káry, Srdce, Piky }
    enum Values { Dvojka, Trojka, Čtyřka, Pětka, Šestka, Sedmička, Osmička, Devítka, Desítka, Kluk, Královna, Král, Eso }
    class PlayingCard
    {
        private readonly Colors color;
        private readonly Values value;

        public PlayingCard(Colors b, Values h)
        {
            this.color = b;
            this.value = h;
        }

        public override string ToString()
        {
            return string.Format("{0} - {1}", this.value, this.color);
        }

        public Colors CardColor()
        {
            return this.color;
        }

        public Values HodnotaKarty()
        {
            return this.value;
        }
    }
}
