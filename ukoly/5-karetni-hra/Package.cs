﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5_karetni_hra
{
    class Package
    {
        public const int countColors = 4;
        public const int countValues = 13;

        private int nCardsInPackage = 0;
        private Random randomCard = new Random();

        private PlayingCard[] cards;

        public Package()
        {
            this.cards = new PlayingCard[ countColors * countValues];
            for (Colors c = Colors.Kříže; c <= Colors.Piky; c++)
            {
                for (Values v = Values.Dvojka; v <= Values.Eso; v++)
                {
                    this.cards[nCardsInPackage++] = new PlayingCard(c, v);
                }
            }

        }

        public PlayingCard SelectCard()
        {
            var n = (int)randomCard.Next(nCardsInPackage);
            PlayingCard karta = cards[n];
            for (var i = n; i < nCardsInPackage - 1; i++)
                cards[i] = cards[i + 1];
            cards[--nCardsInPackage] = null;
            return karta;
        }

    }
}
