﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _5_karetni_hra
{
    public partial class Game : Window
    {
        public const int Players = 4;
        public const int CardsCount = 8;
        public Game()
        {
            InitializeComponent();
        }

        private void btnGive_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Package pack = new Package();
                Player[] players = { new Player(CardsCount), new Player(CardsCount), new Player(CardsCount), new Player(CardsCount) };
                for (var numP = 0; numP < Players; numP++)
                {
                    for (var numC = 0; numC < CardsCount; numC++)
                    {
                        PlayingCard card = pack.SelectCard();
                        players[numP].AddCard(card);
                    }
                }
                first.Text = players[0].ToString();
                second.Text = players[1].ToString();
                third.Text = players[2].ToString();
                fourth.Text = players[3].ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
