﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6_prevody
{
    static class Prevod
    {
        public static int ConvertToBase(this int i, int b)
        {
            if (b < 2 || b > 10)
                throw new ArgumentException("Hodnotu nelze konvertovat do základu " + b.ToString());
            int result = 0;
            int it = 0;

            do
            {
                int next = i % b;
                i /= b;
                result += next * (int)Math.Pow(10, it);
                it++;
            }
            while (i != 0);
            return result;
        }
    }
}
