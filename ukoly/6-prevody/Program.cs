﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6_prevody
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int x = 591;
                for (int i = 2; i <= 10; i++)
                {
                    Console.WriteLine("{0} v soustavě {1} je {2}", x, i, x.ConvertToBase(i));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Výjimka: {0}", ex.Message);
            }
        }

        
    }
}
