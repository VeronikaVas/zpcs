﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7_tuple
{
    class Tuple : IComparable
    {
        private int[] tpl { get; set; }

        public int Size()
        {
            return tpl.Length;
        }

        public int CompareTo(object obj)
        {
            if (obj is Tuple)
            {
                if (this == (Tuple)obj) return 0;
                else if ((Tuple)obj < this) return 1;
                else return -1;
            }
            else
            {
                throw new Exception("Object is not a Tuple");
            }
        }

        public Tuple(int[] arr)
        {
            tpl = arr;
        }

        public static bool operator ==(Tuple a, Tuple b)
        {
            for (int i = 0; i < a.Size(); i++)
            {
                if (a.tpl[i] != b.tpl[i]) return false;
            }
            return true;
        }

        public static bool operator !=(Tuple a, Tuple b)
        {
            for (int i = 0; i < a.Size(); i++)
            {
                if (a.tpl[i] == b.tpl[i]) return false;
            }
            return true;
        }

        public static bool operator >(Tuple a, Tuple b)
        {
            for (int i = 0; i < a.Size(); i++)
            {
                if (a.tpl[i] > b.tpl[i]) return true;
                else if (a.tpl[i] < b.tpl[i]) return false;
            }
            return false;
        }

        public static bool operator <(Tuple a, Tuple b)
        {
            for (int i = 0; i < a.Size(); i++)
            {
                if (a.tpl[i] < b.tpl[i]) return true;
                else if (a.tpl[i] > b.tpl[i]) return false;
            }
            return false;
        }

        public static bool operator >=(Tuple a, Tuple b)
        {
            for (int i = 0; i < a.Size(); i++)
            {
                if (a.tpl[i] > b.tpl[i]) return true;
                else if (a.tpl[i] < b.tpl[i]) return false;
            }
            return true;
        }

        public static bool operator <=(Tuple a, Tuple b)
        {
            for (int i = 0; i < a.Size(); i++)
            {
                if (a.tpl[i] < b.tpl[i]) return true;
                else if (a.tpl[i] > b.tpl[i]) return false;
            }
            return true;
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < tpl.Length; i++)
            {
                builder.Append(tpl[i]).Append(" ");
            }
            return builder.ToString();
        }
    }
}
