﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;


namespace _1_zaklady_klikani
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void bVypis_Click(object sender, RoutedEventArgs e)
        {
            vystupLabel.Content = tJmeno.Text + "\n";
            vystupLabel.Content += tUlice.Text + "\n";
            vystupLabel.Content += tPsc.Text + "\n";
            vystupLabel.Content += tMesto.Text;
        }
    }
}
