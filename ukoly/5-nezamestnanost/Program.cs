﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5_nezamestnanost
{
    class Program
    {
        static Hashtable months = new Hashtable();
        static void Main(string[] args)
        {
            GenerateUnemployment();
            DictionaryEntry max = MaxUnemployment();
            DictionaryEntry min = MinUnemployment();
            double prumer = AverageUnemployment();

            Console.WriteLine("Největší nezaměstnanost byla v měsíci {0} a to {1} %", max.Key, max.Value);
            Console.WriteLine("Nejmenší nezaměstnanost byla v měsíci {0} a to {1} %", min.Key, min.Value);
            Console.WriteLine("Průměrná nezaměstnanost byla {0} %", prumer);
        }

        static void GenerateUnemployment()
        {
            months["Leden"] = 23;
            months["Unor"] = 26;
            months["Brezen"] = 68;
            months["Duben"] = 12;
            months["Kveten"] = 3;
            months["Cerven"] = 47;
            months["Cervenec"] = 60;
            months["Srpen"] = 24;
            months["Zari"] = 35;
            months["Rijen"] = 72;
            months["Listopad"] = 23;
            months["Prosinec"] = 27;
        }

        static double AverageUnemployment()
        {
            double av = 0;
            foreach (DictionaryEntry m in months)
                av += Convert.ToDouble(m.Value);
            return av / 12;
        }

        static DictionaryEntry MaxUnemployment()
        {
            DictionaryEntry max = new DictionaryEntry();
            foreach (DictionaryEntry m in months)
            {
                if (Convert.ToDouble(m.Value) > Convert.ToDouble(max.Value))
                    max = m;
            }
            return max;
        }

        static DictionaryEntry MinUnemployment()
        {
            DictionaryEntry min = new DictionaryEntry("MAX", 100);
            foreach (DictionaryEntry m in months)
            {
                if (Convert.ToDouble(m.Value) <= Convert.ToDouble(min.Value))
                    min = m;
            }
            return min;
        }

    }
}
