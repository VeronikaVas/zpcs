﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _2_aritmeticke_operatory
{
    /// <summary>
    /// Interakční logika pro MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void calculate_Click(object sender, RoutedEventArgs e)
        {
            int lhs = int.Parse(lhsOperand.Text);
            int rhs = int.Parse(rhsOperand.Text);
            if ((bool)addition.IsChecked)
                addValues(lhs, rhs);
            else if ((bool)subtraction.IsChecked)
                subtractValues(lhs, rhs);
            else if ((bool)multiplication.IsChecked)
                multiplyValues(lhs, rhs);
            else if ((bool)division.IsChecked)
                divideValues(lhs, rhs);
            else if ((bool)remainder.IsChecked)
                remainderValues(lhs, rhs);
        }

        private void addValues(int lhs, int rhs)
        {
            int outcome;
            outcome = lhs + rhs;
            expression.Text = lhsOperand.Text + " + " + rhsOperand.Text;
            result.Text = outcome.ToString();
        }

        private void subtractValues(int lhs, int rhs)
        {
            int outcome;
            outcome = lhs - rhs;
            expression.Text = lhsOperand.Text + " - " + rhsOperand.Text;
            result.Text = outcome.ToString();
        }

        private void multiplyValues(int lhs, int rhs)
        {
            int outcome;
            outcome = lhs * rhs;
            expression.Text = lhsOperand.Text + " * " + rhsOperand.Text;
            result.Text = outcome.ToString();
        }

        private void divideValues(int lhs, int rhs)
        {
            int outcome;
            if(rhs == 0)
            {
                result.Text = "Nulou nelze dělit!";
            }
            else
            {
                outcome = lhs / rhs;
                result.Text = outcome.ToString();
            }

            expression.Text = lhsOperand.Text + " / " + rhsOperand.Text;

        }

        private void remainderValues(int lhs, int rhs)
        {
            int outcome;
            outcome = lhs % rhs;
            expression.Text = lhsOperand.Text + " % " + rhsOperand.Text;
            result.Text = outcome.ToString();
        }

        private void quit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
