﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11_linq
{
    class Program
    {
        static void Main(string[] args)
        {
            //suda cisla
            int[] n1 = new int[10] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            var query = from VrNum in n1 where (VrNum % 2) == 0 select VrNum;

            //kladna cisla
            int[] n2 = { 1, 3, -2, -4, -7, -3, -8, 12, 19, 6, 9, 10, 14 };
            var query2 = from num in n2 where num > 0 select num;

            //2. mocnina cisla, ktera je vetsi nez 20
            int[] n3 = new[] { 3, 9, 2, 8, 6, 5 };
            var query3 = from int num in n3
                         let p2 = num * num
                         where p2 > 20
                         select new { num, p2 };

            //cetnost znaku
            string str = "apple";
            var query4 = from x in str
                       group x by x into y
                       select y;
            /*
            foreach (var d in query4)
            {
                Console.WriteLine("Character " + d.Key + ": " + d.Count() + " times");
            }*/


            //serazeni v seznamu
            var itemlist = (from c in Item_Mast.GetItemMast()
                            select c.ItemDes).Distinct().OrderBy(x => x);

            foreach (var item in itemlist)
                Console.WriteLine(item);


        }



        
    }

    class Item_Mast
    {
        public int ItemId { get; set; }
        public string ItemDes { get; set; }

        public static List<Item_Mast> GetItemMast()
        {
            List<Item_Mast> itemlist = new List<Item_Mast>();
            itemlist.Add(new Item_Mast() { ItemId = 1, ItemDes = "Biscuit  " });
            itemlist.Add(new Item_Mast() { ItemId = 2, ItemDes = "Honey    " });
            itemlist.Add(new Item_Mast() { ItemId = 3, ItemDes = "Butter   " });
            itemlist.Add(new Item_Mast() { ItemId = 4, ItemDes = "Brade    " });
            itemlist.Add(new Item_Mast() { ItemId = 5, ItemDes = "Honey    " });
            itemlist.Add(new Item_Mast() { ItemId = 6, ItemDes = "Biscuit  " });
            return itemlist;
        }
    }
}
