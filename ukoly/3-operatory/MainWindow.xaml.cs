﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _3_operatory
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnCopy_Click(object sender, RoutedEventArgs e)
        {
            txtCopy.Text = "";
            string from = txtSource.Text;
            for (int i = 0; i != from.Length; i++)
                txtCopy.Text += copySymbol(from[i]);
        }

        private string copySymbol(char s)
        {
            switch (s)
            {
                case '<':
                    return "&lt;";
                case '>':
                    return "&gt;";
                case '&':
                    return "&amp;";
                case '\"':
                    return "&#34;";
                case '\'':
                    return "&#39;";
                default:
                    return s.ToString();
            }

        }
    }
}
