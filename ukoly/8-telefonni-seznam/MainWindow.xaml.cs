﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _8_telefonni_seznam
{
 
    public partial class MainWindow : Window
    {
        private PhoneBook contacts = new PhoneBook();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnSearchName_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnSearchNumber_Click(object sender, RoutedEventArgs e)
        {
            string text = txtName.Text;
            if (!string.IsNullOrEmpty(text))
            {
                Name j = new Name(text);
                PhoneNumber c = contacts[j];
                txtNumber.Text = c.Number;
            }

        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(txtName.Text) && !String.IsNullOrEmpty(txtNumber.Text))
            {
                contacts.Add(txtName.Text, txtNumber.Text);
                txtName.Text = "";
                txtNumber.Text = "";
            }


        }
    }
}
