﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8_telefonni_seznam
{
    class Name
    {
        public string UName { get; set; }
        public string LastName { get; set; }
        public Name(string fullName)
        {
            UName = "";
            int i = 0;
            while (fullName[i] != ' ') UName += fullName[i++];
            LastName = "";
            i++;
            while (i < fullName.Length) LastName += fullName[i++];
        }

    }
}
