﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8_telefonni_seznam
{
    struct Contact
    {
        public Name name;
        public PhoneNumber number;
    }
    class PhoneBook
    {
        private ArrayList contacts;

        public PhoneBook()
        {
            this.contacts = new ArrayList();
        }

        public void Add(string name, string num)
        {
            Contact c = new Contact
            {
                name = new Name(name),
                number = new PhoneNumber(num)
            };
            contacts.Add(c);
        }

        public PhoneNumber this[Name s]
        {
            get
            {
                foreach (Contact c in contacts)
                    if ((c.name.UName + c.name.LastName) == (s.UName + s.LastName)) 
                        return c.number;
                return null;
            }
        }

        public Name this[PhoneNumber s]
        {
            get
            {
                foreach (Contact c in contacts)
                    if (c.number.Number == s.Number) 
                        return c.name;
                return null;
            }
        }


    }
}
