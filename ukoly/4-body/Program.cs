﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_body
{
    class Program
    {
        static void Main(string[] args)
        {
            Point b1 = new Point();
            Point b2 = new Point(1024, 1280);

            Console.WriteLine(b1.ToString());
            Console.WriteLine(b2.ToString());

            Console.WriteLine("Počet bodů je: {0}", Point.PointsCount());
            double dist = b1.DistanceFrom(b2);
            Console.WriteLine("Vzdálenost od bodu {0} je {1:F}", b2.ToString(), dist);
        }
    }
}
