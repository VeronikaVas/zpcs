﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_body
{
    class Point
    {
        private int x, y;
        private static int count = 0;

        public Point()
        {
            x = 0;
            y = 0;
            count++;
            Console.WriteLine("x={0}, y={1}", x, y);
        }

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
            count++;
            Console.WriteLine("x={0}, y={1}", x, y);
        }

        ~Point()
        {
            Console.WriteLine("Objekt byl zrušen.");
        }

        public override string ToString()
        {
            return "x=" + x + ", y=" + y; 
        }

        public static int PointsCount()
        {
            return count;
        }

        public double DistanceFrom(Point p)
        {
            int xDiff = this.x - p.x;
            int yDiff = this.y - p.y;
            return Math.Sqrt((xDiff * xDiff) + (yDiff * yDiff));
        }

    }
}
