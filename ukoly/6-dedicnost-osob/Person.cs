﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6_dedicnost_osob
{
    class Person
    {
        protected string name;
        protected string rc;

        public Person(string name, string rc)
        {
            this.name = name;
            this.rc = rc;
        }

        public string Gender()
        {
            return rc[2] - '0' > 1 ? "Žena" : "Muž";
        }

        public void ChangeName(string name)
        {
            this.name = name;
        }

        public void ChangeRC(string rc)
        {
            this.rc = rc;
        }

        public virtual void Print()
        {
            Console.WriteLine("Jmeno: " + name);
            Console.WriteLine("RČ: " + rc);
            Console.WriteLine("Pohlaví: " + Gender());
        }
    }
}
