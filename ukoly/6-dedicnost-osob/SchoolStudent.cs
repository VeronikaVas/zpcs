﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6_dedicnost_osob
{
    class SchoolStudent : Person
    {
        public string school;
        public SchoolStudent(string name, string rc, string school) : base(name, rc)
        {
            this.school = school;
        }

        public void SetSchool(string school)
        {
            this.school = school;
        }

        public override void Print()
        {
            base.Print();
            Console.WriteLine("Skola: " + school);
        }
    }
}
