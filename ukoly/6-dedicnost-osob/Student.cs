﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6_dedicnost_osob
{
    class Student : SchoolStudent
    {
        protected string field;

        public Student(string jmeno, string rc, string school, string field) : base(jmeno, rc, school)
        {
            this.field = field;
        }

        public override void Print()
        {
            base.Print();
            Console.WriteLine("Obor: " + field);
        }
    }
}
