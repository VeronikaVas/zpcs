﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9_mnozina
{
    class Set
    {
        private ArrayList items = null;

        public Set(ArrayList items = null)
        {
            if (items == null)
                items = new ArrayList();
            this.items = items;
        }

        public static Set operator >>(Set m, int c)
        {
            m.items.Remove(c);
            return m;
        }

        public static Set operator <<(Set m, int c)
        {
            m.items.Add(c);
            return m;
        }

        public static bool operator !(Set m)
        {
            return m.items.Count <= 0;
        }

        public static int operator +(Set m)
        {
            return m.items.Count;
        }

        public static Set operator +(Set a, Set b)
        {
            ArrayList c = new ArrayList();
            c.AddRange(a.items);
            c.AddRange(b.items);
            return new Set(c);
        }

        public static Set operator -(Set a, Set b)
        {
            ArrayList rozdil = new ArrayList();
            rozdil.AddRange(a.items);
            foreach (int x in b.items)
            {
                rozdil.Remove(x);
            }
            return new Set(rozdil);
        }

        public override string ToString()
        {
            string s = "[ ";
            foreach (int x in this.items)
                s += x + " ";
            s += "]";
            return s;
        }
    }
}
