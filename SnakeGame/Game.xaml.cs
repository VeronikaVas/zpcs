﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SnakeGame
{
    public partial class Game : Window
    {
        private const int SnakeSize = 6;
        private const int FoodSize = 4;

        private List<Point> foodPoints;
        private int score = 0;

        private readonly Random rnd;

        private Point startingPosition;
        private Point currentPosition;

        private Direction direction;

        private readonly Brush snakeColor = Brushes.Green;
        private List<Point> snakePoints;
        private int snakeLength;

        DispatcherTimer timer;

        private enum Direction
        {
            Up = 1,
            Down = 2,
            Left = 3,
            Right = 4
        };

        public Game()
        {
            InitializeComponent();
            rnd = new Random();

            NewGame();
        }

        private void NewGame()
        {
            PaintCanvas.Children.Clear();

            score = 0;
            snakeLength = 50;
            direction = 0;

            txtScore.Text = "0";

            snakePoints = new List<Point>();

            timer = new DispatcherTimer();
            timer.Tick += new EventHandler(Play);
            timer.Interval = new TimeSpan(10000);
            timer.Start();

            this.KeyDown += new KeyEventHandler(OnButtonKeyDown);

            startingPosition = new Point(250, 100);
            PaintSnake(startingPosition);
            currentPosition = new Point();
            currentPosition = startingPosition;

            foodPoints = new List<Point>();

            for (var i = 0; i < 10; i++)
            {
                PaintFood(i);
            }
        }

        private void PaintFood(int index)
        {
            Point bonusPoint = new Point(rnd.Next(5, 780), rnd.Next(5, 480));

            Ellipse newEllipse = new Ellipse
            {
                Fill = Brushes.OrangeRed,
                Width = FoodSize,
                Height = FoodSize
            };

            Canvas.SetTop(newEllipse, bonusPoint.Y);
            Canvas.SetLeft(newEllipse, bonusPoint.X);
            PaintCanvas.Children.Insert(index, newEllipse);
            foodPoints.Insert(index, bonusPoint);
        }

        private void PaintSnake(Point currentposition)
        {

            Ellipse newEllipse = new Ellipse
            {
                Fill = snakeColor,
                Width = SnakeSize,
                Height = SnakeSize
            };

            Canvas.SetTop(newEllipse, currentposition.Y);
            Canvas.SetLeft(newEllipse, currentposition.X);

            int count = PaintCanvas.Children.Count;
            PaintCanvas.Children.Add(newEllipse);
            snakePoints.Add(currentposition);

            if (count > snakeLength)
            {
                PaintCanvas.Children.RemoveAt(count - snakeLength + 9);
                snakePoints.RemoveAt(count - snakeLength);
            }
        }

        private void Play(object sender, EventArgs e)
        {

            switch (direction)
            {
                case Direction.Down:
                    currentPosition.Y += 1;
                    PaintSnake(currentPosition);
                    break;
                case Direction.Up:
                    currentPosition.Y -= 1;
                    PaintSnake(currentPosition);
                    break;
                case Direction.Left:
                    currentPosition.X -= 1;
                    PaintSnake(currentPosition);
                    break;
                case Direction.Right:
                    currentPosition.X += 1;
                    PaintSnake(currentPosition);
                    break;
            }

            if ((currentPosition.X < 5) || (currentPosition.X > 780) ||
                (currentPosition.Y < 5) || (currentPosition.Y > 470))
            {
                GameOver();
                return;
            }
                
            int n = 0;
            foreach (Point point in foodPoints)
            {
                if ((Math.Abs(point.X - currentPosition.X) < FoodSize) &&
                    (Math.Abs(point.Y - currentPosition.Y) < FoodSize))
                {
                    snakeLength += 10;
                    UpdateScore();

                    foodPoints.RemoveAt(n);
                    PaintCanvas.Children.RemoveAt(n);
                    PaintFood(n);
                    break;
                }
                n++;
            }

            for (var q = 0; q < (snakePoints.Count - SnakeSize * 2); q++)
            {
                Point point = new Point(snakePoints[q].X, snakePoints[q].Y);
                if ((Math.Abs(point.X - currentPosition.X) < (SnakeSize)) &&
                     (Math.Abs(point.Y - currentPosition.Y) < (SnakeSize)))
                {
                    GameOver();
                    return;
                }

            }

        }

        private void OnButtonKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Down:
                    direction = Direction.Down;
                    break;
                case Key.Up:
                    direction = Direction.Up;
                    break;
                case Key.Left:
                    direction = Direction.Left;
                    break;
                case Key.Right:
                    direction = Direction.Right;
                    break;

            }
        }

        private void UpdateScore()
        {
            score += 10;
            txtScore.Text = score.ToString();
        }

        private void GameOver()
        {
            timer.Stop();
            if (MessageBox.Show("Prohrál jsi! Chceš spustit novou hru?", "Hra", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                NewGame();
            }
            else
            {
                this.Close();
            }
        }

        private void btnNewGame_Click(object sender, RoutedEventArgs e)
        {
            timer.Stop();
            NewGame();
        }
    }
}
