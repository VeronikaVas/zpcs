﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleship
{
    public static class Extensions
    {
        public static Point CalculatePointToBoard(this Point mousePosition, Size sz, int boardSize)
        {
            return new Point(mousePosition.X / (sz.Width / boardSize), mousePosition.Y / (sz.Height / boardSize));
        }
    }
}
