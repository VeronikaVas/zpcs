﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleship
{
    class Ship : Field
    {
        ShipTypes type;
        
        

        public Ship(int t)
        {
            this.type = (ShipTypes)t;
            Attacked = false;
        }

        public Color Color
        {
            get { return Color.LightBlue; }
        }

        public override void Draw(Graphics g, Point p, Size sz)
        {
            if (Attacked)
            {
                g.FillRectangle(new SolidBrush(Color.Red), new Rectangle(p.X + 1, p.Y + 1, sz.Width - 2, sz.Height - 2));
            }

            else if (Hidden)
            {
                g.FillRectangle(new SolidBrush(Color.White), new Rectangle(p.X + 1, p.Y + 1, sz.Width - 2, sz.Height - 2));
            }



            else
            {
                g.FillRectangle(new SolidBrush(Color), new Rectangle(p.X + 1, p.Y + 1, sz.Width - 2, sz.Height - 2));
            }

            base.Draw(g, p, sz);
        }

        public enum ShipTypes
        {

            PATROL = 2,
            SUBMARINE = 3,
            DESTROYER = 3,
            BATTLESHIP = 4,
            AIRCRAFT = 5
        }


        public int GetTypeOfShip()
        {
            return (int)type;
        }


    }
}
