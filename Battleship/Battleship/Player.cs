﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleship
{
    public class Player : IPlayer
    {
        public event Action<string> OnMessage = delegate { };
        private GameBoard GameBoard { get; set; }
        private string Name { get; set; }

        private int currentShip;
        public const int NUMBER_OF_BOATS = 5;

        public int[] ships = new int[NUMBER_OF_BOATS] { 5, 4, 3, 3, 2 };

        public Player(string name)
        {
            this.Name = name;

            GameBoard = new GameBoard();
            currentShip = 0;

        }

        public Player()
        {
            //this.Name = name;

            GameBoard = new GameBoard();
            currentShip = 0;

        }

        public GameBoard getBoard()
        {
            return GameBoard;
        }

        public string getName()
        {
            return this.Name;
        }



        public bool HasSomeBoats()
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {

                    if (!GameBoard.IsAttacked(i, j) && GameBoard.HasShip(i, j))
                    {
                        return true;
                    }

                }
            }
            OnMessage("Výhra!");
            return false;
        }

        public bool hasShipToPlace => (currentShip < NUMBER_OF_BOATS);

        public void SetNextShip()
        {
            this.currentShip++;
        }
       
        public bool PlaceShip(int i, int r, int c, bool horizontal){
            if (!GameBoard.HasShip(r, c)) {
			if (hasShipToPlace) {
				if (GameBoard.SetShipOnBoard(ships[i], r , c, horizontal)) {
                    OnMessage("Loď vložena.");
					SetNextShip();
                    return true;
				}

				else {
                    OnMessage("Lodě se dotýkají.");
                    return false;
				}
			}

			else {
                OnMessage("Všechny lodě uloženy do desky!");
                return false;
			}

		} else {
			
                OnMessage("Pozice je již obsazena, zkuste to znovu.");
                return false;
		}
	}

        public void Attack(int r, int c, IPlayer enemy) {
		if (enemy.getBoard().Shot(r,c)) {
            if (enemy.getBoard()[r, c] is Ship)
                    OnMessage(string.Format("Zasáhli jste nepřítelovu loď - {0},{1}", r, c));
		} else {
            OnMessage("Samá voda");
		    }
	    }

        
    }
}
