﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Battleship
{
    public class GameControl
    {
        public event Action<string> OnMessage1 = delegate { };
        //public event Action<string> PlayerWon = delegate { };
        public readonly int SIZE = 10;
        public int type;


        private IPlayer player1;
        private IPlayer player2{ get; set; }

        private IPlayer actualPlayer { get; set; }
        private IPlayer enemy { get; set; }

        public GameControl()
        {
            type = 0;
        }


        public void SetPlayers(string player1, string player2, int typeGame)
        {
            if (typeGame == 1)
            {
                this.player1 = new Player(player1);
                this.player2 = new Player(player2);
                this.type = typeGame;
            }

            else if (typeGame == 2)
            {
                this.player1 = new Player(player1);
                this.player2 = new Computer(player2);
                this.type = typeGame;
            }

            this.actualPlayer = this.player1;
            this.enemy = this.player2;
            this.player1.OnMessage += player1_OnMessage;

            
        }

        void player1_OnMessage(string obj)
        {
            OnMessage1(obj);
        }

        public String GetAttackPlayerName()
        {
            return actualPlayer.getName();
        }

        public String GetDefendPlayerName()
        {
            return enemy.getName();
        }

        public IPlayer GetAttackPlayer()
        {
            return actualPlayer;
        }

        public IPlayer GetDefendPlayer()
        {
            return enemy;
        }



        public Image GetImageActualPlayer(Size sz)
        {
            Image image = new Bitmap(sz.Width, sz.Height);
            int w = sz.Width / SIZE;
            int h = sz.Height / SIZE;

           

            using (Graphics g = Graphics.FromImage(image))
            {
                for (int x = 0; x < actualPlayer.getBoard().BOARD_SIZE; x++)
                    for (int y = 0; y < actualPlayer.getBoard().BOARD_SIZE; y++)
                    {
                        Point p = new Point(x * w, y * h);
                        Size s = new Size(w, h);
                        if (actualPlayer.getBoard().HasShip(x, y) && !actualPlayer.getBoard()[x, y].Attacked)
                        {
                            actualPlayer.getBoard()[x, y].Hidden = false;
                        }

                        if (actualPlayer.getBoard().HasShip(x, y) && actualPlayer.getBoard()[x, y].Attacked)
                        {
                            actualPlayer.getBoard()[x, y].Hidden = false;
                            g.FillRectangle(new SolidBrush(Color.Red), new Rectangle(p.X + 1, p.Y + 1, s.Width - 2, s.Height - 2));
                        }



                          g.FillRectangle(new SolidBrush(Color.White), new Rectangle(p.X + 1, p.Y + 1, s.Width - 2, s.Height - 2));
                          actualPlayer.getBoard()[x, y].Draw(g, p, s);
                        
                            

                    }
                        
            }
            return image;
        }

        public Image GetImageEnemy(Size sz)
        {
            Image image = new Bitmap(sz.Width, sz.Height);
            int w = sz.Width / SIZE;
            int h = sz.Height / SIZE;

            using (Graphics g = Graphics.FromImage(image))
            {
                for (int x = 0; x < enemy.getBoard().BOARD_SIZE; x++)
                    for (int y = 0; y < enemy.getBoard().BOARD_SIZE; y++)
                    {
                        Point p = new Point(x * w, y * h);
                        Size s = new Size(w, h);

                        if (enemy.getBoard().HasShip(x, y) && !enemy.getBoard()[x, y].Attacked)
                        {
                            enemy.getBoard()[x, y].Hidden = true;

                        }

                        if (enemy.getBoard().HasShip(x, y) && enemy.getBoard()[x, y].Attacked)
                        {
                            enemy.getBoard()[x, y].Hidden = false;
                            //g.FillRectangle(new SolidBrush(Color.White), new Rectangle(p.X + 1, p.Y + 1, s.Width - 2, s.Height - 2));
                        }



                        g.FillRectangle(new SolidBrush(Color.White), new Rectangle(p.X + 1, p.Y + 1, s.Width - 2, s.Height - 2));
                        enemy.getBoard()[x, y].Draw(g, p, s);
                    }

            }
            return image;
        }

        public Image GetImageHide(Size sz)
        {
            Image image = new Bitmap(sz.Width, sz.Height);
            int w = sz.Width / SIZE;
            int h = sz.Height / SIZE;

            using (Graphics g = Graphics.FromImage(image))
            {
                for (int x = 0; x < actualPlayer.getBoard().BOARD_SIZE; x++)
                    for (int y = 0; y < actualPlayer.getBoard().BOARD_SIZE; y++)
                    {
                        Point p = new Point(x * w, y * h);
                        Size s = new Size(w, h);

                        if (actualPlayer.getBoard().HasShip(x, y) && !actualPlayer.getBoard()[x, y].Attacked)
                        {
                            actualPlayer.getBoard()[x, y].Hidden = true;

                        }

                        if (actualPlayer.getBoard().HasShip(x, y) && actualPlayer.getBoard()[x, y].Attacked)
                        {
                            actualPlayer.getBoard()[x, y].Hidden = true;
                        }



                        g.FillRectangle(new SolidBrush(Color.White), new Rectangle(p.X + 1, p.Y + 1, s.Width - 2, s.Height - 2));
                        enemy.getBoard()[x, y].Draw(g, p, s);
                    }

            }
            return image;
        }


        public void SwapPlayers()
        {
            if (actualPlayer == player1)
            {
                actualPlayer = player2;
                enemy = player1;
            }
            else
            {
                actualPlayer = player1;
                enemy = player2;
            }
        }

        internal bool Turn(Point p)
        {
            
            actualPlayer.Attack(p.X, p.Y, enemy);

            if (!enemy.HasSomeBoats())
            {
                return true;
            }
            return false;
        }

        internal void ComputerTurn()
        {
            enemy.Attack(0, 0, actualPlayer);
        }
    }
}
