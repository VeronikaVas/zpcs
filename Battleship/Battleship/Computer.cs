﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Battleship
{
    public class Computer : IPlayer
    {
        public event Action<string> OnMessage = delegate { };

        private  List<Field> attackingBoat;
        private  List<Field> possibleShot;
        private bool state;
    
        private Random random;

        private GameBoard GameBoard { get; set; }
        private string Name { get; set; }

        private int currentShip = 0;
        public const int NUMBER_OF_BOATS = 5;

        public int[] ships = new int[NUMBER_OF_BOATS]{ 5, 4, 3, 3, 2 };


        public Computer(string name)
        {

            this.Name = name;

            state = false;
            random = new Random();
            possibleShot = new List<Field>();
            attackingBoat = new List<Field>();
            GameBoard = new GameBoard();
            for (int i = 0; i < GameBoard.BOARD_SIZE; i++)
            {
                for (int j = 0; j < GameBoard.BOARD_SIZE; j++)
                {
                    Field f = new Field(i, j);
                    possibleShot.Add(f);
                }
            }
              }


        public GameBoard getBoard()
        {
            return GameBoard;
        }

        public String getName()
        {
            return this.Name;
        }



        public bool HasSomeBoats()
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {

                    if (!GameBoard.IsAttacked(i, j) && GameBoard.HasShip(i, j))
                    {
                        return true;
                    }

                }
            }
            OnMessage("Výhra!");
            return false;
        }

        public bool hasShipToPlace => (currentShip < NUMBER_OF_BOATS);

        public void SetNextShip()
        {
            this.currentShip++;
        }


        public bool PlaceShip(int size, int x, int y, bool a)
        {

            if (GameBoard.SetShipOnBoard(ships[currentShip], random.Next(9),
                    random.Next(9), (random.Next(100) < 20 ? true : false)))
            {
                SetNextShip();
                return true;
            }
            return false;
        }

        public void Attack(int x, int y,IPlayer enemy) {
		    if (NextShot(enemy.getBoard())) {
			    OnMessage("Vaše loď byla zasažena!");
		    } else {
			    OnMessage("Uff!");
		    }
	    }

		
	
	private Field GetNeighbour() {
		Field f;
		//soused vlevo
        f = new Field(attackingBoat.ElementAt(0).getX(), attackingBoat.ElementAt(0).getY() - 1);
        if (IsShotable(f) != -1 && IsLegalMove(f)) {
                return f;
        }
        
        //soused vpravo
        f = new Field(attackingBoat.ElementAt(0).getX(), attackingBoat.ElementAt(0).getY() + 1);
        if (IsShotable(f) != -1 && IsLegalMove(f)) {
                return f;
        }
        
        //soused nahore
        f = new Field(attackingBoat.ElementAt(0).getX() + 1, attackingBoat.ElementAt(0).getY());
        if (IsShotable(f) != -1 && IsLegalMove(f)) {
                return f;
        }
        
        //soused dole
        f = new Field(attackingBoat.ElementAt(0).getX() - 1, attackingBoat.ElementAt(0).getY());
        if (IsShotable(f) != -1 && IsLegalMove(f)) {
                return f;
        }
        return null;
}
	
	

	public bool NextShot(GameBoard board) {
		Field f;
        bool hit;

		if (state) {
			
			if (attackingBoat.Count() == 1) {
				f = GetNeighbour();
				if (GetNeighbour() != null && IsShotable(GetNeighbour()) != -1) {
					possibleShot.RemoveAt(IsShotable(GetNeighbour()));
					hit = board.Shot(f.getX(), f.getY());
                    if ( board.HasShip(f.getX(), f.getY()) && hit)
                    {
						attackingBoat.Add(f);
						if ((GetDirection() && attackingBoat.ElementAt(0).getY() > attackingBoat
								.ElementAt(1).getY())
								|| !GetDirection()
								&& attackingBoat.ElementAt(0).getX() > attackingBoat
										.ElementAt(1).getX()) {

                               Field r = attackingBoat.ElementAt(0);
                                //Console.WriteLine(f.getX() + f.getY());
                                            attackingBoat.RemoveAt(0);
                           attackingBoat.Add(r);
                            
						}
						
						return true;
					} else {// nezasazen

						return false;
					}
				} else {
					state = false;
					NextShot(board);

				}

			} else {// +1
				f = GetField();
				CheckAndRemove();
				if (f != null) {
                    f = possibleShot.ElementAt(IsShotable(f));
                        possibleShot.RemoveAt(IsShotable(f));
					hit = board.Shot(f.getX(), f.getY());
                    if (hit && board.HasShip(f.getX(), f.getY()))
                    {
						return true;
					} else {
						if (f.getX() == attackingBoat.ElementAt(0).getX()
								&& f.getY() == attackingBoat.ElementAt(0)
										.getY()) {
							attackingBoat.RemoveAt(0);
						} else {
							attackingBoat.RemoveAt(attackingBoat.Count() - 1);
						}

						return false;
					}

				} else { // p == null
					state = false;
				}
			}
		}

		else if (!state) {
			int index = random.Next(possibleShot.Count());
           
            f = possibleShot.ElementAt(index);
             possibleShot.RemoveAt(index);
             hit = board.Shot(f.getX(), f.getY());
            Console.WriteLine(GameBoard.HasShip(f.getX(), f.getY()));
            Console.WriteLine(f.getX() );
             Console.WriteLine(f.getY());
            if (hit && board.HasShip(f.getX(), f.getY()))
            {
				state = true;
				attackingBoat.Clear();
				attackingBoat.Add(f);
				return true;
			}
			return false;
		}

		return false;
	}


	private void CheckAndRemove() {
        for (int i = 1; i < attackingBoat.Count(); i++) {
                if (attackingBoat.ElementAt(i).getX() == attackingBoat.ElementAt(i - 1).getX()
                                && attackingBoat.ElementAt(i).getY() == attackingBoat.ElementAt(i - 1).getY()) {
                        attackingBoat.RemoveAt(i);
                }
        }
}
	private Field GetField() {
        if (GetDirection()) {
        	Field f = attackingBoat.ElementAt(0);
        	f.SetY(f.getY() - 1);
                if (IsShotable(f) != -1 && IsLegalMove(f)) {
                        attackingBoat.Insert(0, f);
                        return f;
                } else {
                        f = attackingBoat.ElementAt(attackingBoat.Count() - 1);
                        f.SetY(f.getY() + 1);
                        if (IsShotable(f) != -1 && IsLegalMove(f)) {
                                attackingBoat.Add(f);
                                return f;
                        } else {
                                return null;
                        }
                }
        } else {
                Field f = attackingBoat.ElementAt(0);
                f.SetX(f.getX() - 1);
                if (IsShotable(f) != -1 && IsLegalMove(f)) {
                        attackingBoat.Insert(0, f);
                        return f;
                } else {
                        f = attackingBoat.ElementAt(attackingBoat.Count() - 1);
                        f.SetX(f.getX() + 1);
                        if (IsShotable(f) != -1 && IsLegalMove(f)) {
                                attackingBoat.Add(f);
                                return f;
                        } else {
                                return null;
                        }
                }
        }
}
	
	private bool GetDirection() {
        return (attackingBoat.ElementAt(0).getX() == attackingBoat.ElementAt(1).getX());
	}
	
	private bool IsLegalMove(Field f) {
        return (f.getX() > 0 && f.getX() < GameBoard.BOARD_SIZE 
        		&& f.getY() > 0 && f.getY() < GameBoard.BOARD_SIZE);
	}
	
	private int IsShotable(Field f) {
        for (int i = 0; i < possibleShot.Count(); i++) {
                if (possibleShot.ElementAt(i).getX() == f.getX() && possibleShot.ElementAt(i).getY() == f.getY()) {
                        return i;
                }
        }
        return -1;
	}

 

    }
}
