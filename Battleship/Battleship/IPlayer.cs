﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleship
{
    public interface IPlayer
    {
        event Action<string> OnMessage;
        String getName();
        GameBoard getBoard();
        bool HasSomeBoats();
        bool hasShipToPlace { get; }

        bool PlaceShip(int size, int r, int c, bool horizontal);

         void Attack(int p1, int p2, IPlayer playerForDefend);



    }
}
