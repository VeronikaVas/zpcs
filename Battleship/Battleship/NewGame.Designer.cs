﻿namespace Battleship
{
    partial class NewGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label = new System.Windows.Forms.Label();
            this.rbPlayerVsPlayer = new System.Windows.Forms.RadioButton();
            this.tbP1 = new System.Windows.Forms.TextBox();
            this.tbP2 = new System.Windows.Forms.TextBox();
            this.labelPlayer1 = new System.Windows.Forms.Label();
            this.labelPlayer2 = new System.Windows.Forms.Label();
            this.rbPlayerVsComputer = new System.Windows.Forms.RadioButton();
            this.labelPlayer = new System.Windows.Forms.Label();
            this.tbP = new System.Windows.Forms.TextBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label.Location = new System.Drawing.Point(12, 9);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(154, 24);
            this.label.TabIndex = 4;
            this.label.Text = "Zadejte typ hry:";
            // 
            // rbPlayerVsPlayer
            // 
            this.rbPlayerVsPlayer.AutoSize = true;
            this.rbPlayerVsPlayer.Location = new System.Drawing.Point(16, 50);
            this.rbPlayerVsPlayer.Name = "rbPlayerVsPlayer";
            this.rbPlayerVsPlayer.Size = new System.Drawing.Size(89, 17);
            this.rbPlayerVsPlayer.TabIndex = 5;
            this.rbPlayerVsPlayer.TabStop = true;
            this.rbPlayerVsPlayer.Text = "Hráč vs. hráč";
            this.rbPlayerVsPlayer.UseVisualStyleBackColor = true;
            this.rbPlayerVsPlayer.CheckedChanged += new System.EventHandler(this.rbPlayerVsPlayer_CheckedChanged);
            // 
            // tbP1
            // 
            this.tbP1.Location = new System.Drawing.Point(102, 73);
            this.tbP1.Name = "tbP1";
            this.tbP1.Size = new System.Drawing.Size(147, 20);
            this.tbP1.TabIndex = 6;
            // 
            // tbP2
            // 
            this.tbP2.Location = new System.Drawing.Point(102, 99);
            this.tbP2.Name = "tbP2";
            this.tbP2.Size = new System.Drawing.Size(147, 20);
            this.tbP2.TabIndex = 7;
            // 
            // labelPlayer1
            // 
            this.labelPlayer1.AutoSize = true;
            this.labelPlayer1.Location = new System.Drawing.Point(42, 76);
            this.labelPlayer1.Name = "labelPlayer1";
            this.labelPlayer1.Size = new System.Drawing.Size(54, 13);
            this.labelPlayer1.TabIndex = 8;
            this.labelPlayer1.Text = "Hráč č. 1:";
            // 
            // labelPlayer2
            // 
            this.labelPlayer2.AutoSize = true;
            this.labelPlayer2.Location = new System.Drawing.Point(42, 102);
            this.labelPlayer2.Name = "labelPlayer2";
            this.labelPlayer2.Size = new System.Drawing.Size(54, 13);
            this.labelPlayer2.TabIndex = 9;
            this.labelPlayer2.Text = "Hráč č. 2:";
            // 
            // rbPlayerVsComputer
            // 
            this.rbPlayerVsComputer.AutoSize = true;
            this.rbPlayerVsComputer.Location = new System.Drawing.Point(16, 144);
            this.rbPlayerVsComputer.Name = "rbPlayerVsComputer";
            this.rbPlayerVsComputer.Size = new System.Drawing.Size(105, 17);
            this.rbPlayerVsComputer.TabIndex = 10;
            this.rbPlayerVsComputer.TabStop = true;
            this.rbPlayerVsComputer.Text = "Hráč vs. počítač";
            this.rbPlayerVsComputer.UseVisualStyleBackColor = true;
            this.rbPlayerVsComputer.CheckedChanged += new System.EventHandler(this.rbPlayerVsComputer_CheckedChanged);
            // 
            // labelPlayer
            // 
            this.labelPlayer.AutoSize = true;
            this.labelPlayer.Location = new System.Drawing.Point(42, 174);
            this.labelPlayer.Name = "labelPlayer";
            this.labelPlayer.Size = new System.Drawing.Size(33, 13);
            this.labelPlayer.TabIndex = 11;
            this.labelPlayer.Text = "Hráč:";
            // 
            // tbP
            // 
            this.tbP.Location = new System.Drawing.Point(102, 171);
            this.tbP.Name = "tbP";
            this.tbP.Size = new System.Drawing.Size(147, 20);
            this.tbP.TabIndex = 12;
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(142, 215);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(107, 23);
            this.buttonOK.TabIndex = 13;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // NewGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(269, 258);
            this.ControlBox = false;
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.tbP);
            this.Controls.Add(this.labelPlayer);
            this.Controls.Add(this.rbPlayerVsComputer);
            this.Controls.Add(this.labelPlayer2);
            this.Controls.Add(this.labelPlayer1);
            this.Controls.Add(this.tbP2);
            this.Controls.Add(this.tbP1);
            this.Controls.Add(this.rbPlayerVsPlayer);
            this.Controls.Add(this.label);
            this.Name = "NewGame";
            this.Text = "Nová hra";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label;
        private System.Windows.Forms.RadioButton rbPlayerVsPlayer;
        private System.Windows.Forms.TextBox tbP1;
        private System.Windows.Forms.TextBox tbP2;
        private System.Windows.Forms.Label labelPlayer1;
        private System.Windows.Forms.Label labelPlayer2;
        private System.Windows.Forms.RadioButton rbPlayerVsComputer;
        private System.Windows.Forms.Label labelPlayer;
        private System.Windows.Forms.TextBox tbP;
        private System.Windows.Forms.Button buttonOK;
    }
}