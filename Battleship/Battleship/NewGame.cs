﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Battleship
{
    public partial class NewGame : Form
    {
        private GameControl control;
        public NewGame(ref GameControl c)
        {
            InitializeComponent();
            rbPlayerVsPlayer.Checked = true;
            tbP1.Enabled = true;
            tbP2.Enabled = true;
            rbPlayerVsComputer.Checked = false;
            tbP.Enabled = false;

            this.control = c;
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        public GameControl getControl()
        {
            return control;
        }

        private void rbPlayerVsPlayer_CheckedChanged(object sender, EventArgs e)
        {
            if (rbPlayerVsPlayer.Checked)
            {
                tbP1.Enabled = true;
                tbP2.Enabled = true;
            }

            else
            {
                tbP1.Enabled = false;
                tbP2.Enabled = false;
            }
        }

        private void rbPlayerVsComputer_CheckedChanged(object sender, EventArgs e)
        {
            if (rbPlayerVsComputer.Checked)
            {
                tbP.Enabled = true;
            }

            else
            {
                tbP.Enabled = false;
            }



        }

        private void buttonOK_Click(object sender, EventArgs e)
        {

            if(rbPlayerVsPlayer.Checked){
                
                 control.SetPlayers(tbP1.Text, tbP2.Text, 1);
                 
            }
            else if (rbPlayerVsComputer.Checked)
            {
                control.SetPlayers(tbP.Text, "Počítač", 2);
               
                
            }

            Close();
           
        }
    }
}
