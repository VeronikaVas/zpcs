﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleship
{
    public class Field 
    {
        private int x;
        private int y;
        public bool Attacked {get;set;}

        public bool Hidden {get; set;}

        public Field(int x, int y)
        {
            this.x = x;
            this.y = y;

            Attacked = false;
        }

        public int getX(){
        return this.x;
    }
        public int getY()
        {
            return this.y;
        }

        public void SetX(int x)
        {
            this.x = x;
        }
        public void SetY(int y)
        {
             this.y = y;
        }


        public Field()
        {
            this.x = 0;
            this.y = 0;
        }
        public virtual void Draw(Graphics g, Point p, Size sz)
        {
            g.DrawRectangle(Pens.Black, new Rectangle(p.X, p.Y, sz.Width - 1, sz.Height - 1));

            if (Attacked)
            {
                g.DrawLine(Pens.Black, p.X + 5, p.Y + 5, p.X + sz.Width - 5, p.Y + sz.Height - 5);
                g.DrawLine(Pens.Black, p.X + sz.Width - 5, p.Y + 5, p.X + 5, p.Y + sz.Height - 5);
            }
        }
    }
}
