﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleship
{
    public class GameBoard : IEnumerable
    {
        private Field[,] board;
        public int BOARD_SIZE = 10;

        public Field this[int x, int y]
        {
            get
            {
                    return board[x, y];

            }
            set
            {
                    board[x, y] = value;
                
            }
        }

           public GameBoard()
        {
            board = new Field[BOARD_SIZE, BOARD_SIZE];
            InitializeBoard();

        }

        private void InitializeBoard()
        {
            for (int x = 0; x < BOARD_SIZE; x++)
                for (int y = 0; y < BOARD_SIZE; y++)
                {
                    board[x, y] = new Field(x, y);
                   
                }
                   
        }



        private bool ValidPosition(int shipSize, int r, int c, bool horizontal)
        {
            int size = shipSize;

		if (horizontal) {
			
			if (r + size <= BOARD_SIZE) {
				for (int i = r; i < r + size; i++) {
					if (HasShip(i, c)) {
						return false;
					}

                    if (c != 0 && HasShip(i, c - 1))
                    {
						return false;
					}

                    if (c != 9 && HasShip(i, c + 1))
                    {
						return false;
					}
			}
                if (r != 0 && HasShip(r - 1, c))
                {
					return false;
				}
				
				if ((r + size) < BOARD_SIZE && HasShip(r + size,c)) {
					return false;
				}
				return true;	} 
			else {
				return false;
			}
		} 
		else {
			if (c + size <= BOARD_SIZE) {
				for (int i = c; i < c + size; i++) {
					if (HasShip(r,i)) {
						return false;
					}

                    if (r != 0 && HasShip(r - 1, i))
                    {
						return false;
					}
					
					if (r != 9 && HasShip(r + 1,i)) {
						return false;
					}
	
				}

                if (c != 0 && HasShip(r, c - 1))
                {
					return false;
				}
                if ((c + size + 1) <= BOARD_SIZE && HasShip(r, c + size))
                {
					return false;
				}

				

				return true;	
			} else {
				return false;
			}
		}
	}

        public bool HasShip(int x, int y)
        {
            return (board[x, y] is Ship);
        }

        public bool SetShipOnBoard(int shipSize, int r, int c, bool horizontal)
        {
            if (ValidPosition(shipSize, r, c, horizontal))
            {
                
                
                for (int i = 0; i < shipSize; i++)
                {
                    if (horizontal)
                    {
                        board[r + i, c] = new Ship(shipSize);
                    }
                    else
                    {
                        board[r, c + i] = new Ship(shipSize);
                    }
                }
                return true;
            }
            return false;
        }

        private bool IsInRange(int x, int y)
        {
            return (x >= 0 &&
                y >= 0 &&
                x < BOARD_SIZE &&
                y < BOARD_SIZE);
        }

        public bool IsAttacked(int x, int y)
        {
            return board[x, y].Attacked;
        }

        public bool Shot(int r, int c){
		if (!IsAttacked(r, c)) {
            this[r, c].Attacked = true;
			return true;
		}
        return false;

	}



        public IEnumerator GetEnumerator()
        {
            return board.GetEnumerator();
        }
    }
}
