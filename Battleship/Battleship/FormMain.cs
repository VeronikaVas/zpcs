﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Battleship
{
    public partial class FormMain : Form
    {
        private GameControl gameController = new GameControl();

        


        public FormMain()
        {
            InitializeComponent();

            listBoxShips.Items.Add("Aircraft");
            listBoxShips.Items.Add("Battleship");
            listBoxShips.Items.Add("Submarine");
            listBoxShips.Items.Add("Destroyer");
            listBoxShips.Items.Add("Patrol");

            gameController.OnMessage1 += gameController_OnMessage1;
            

            cbDirection.Items.Add("Horizontálně");
            cbDirection.Items.Add("Vertikálně");
            
            buttonAddShip.Enabled = false;
            listBoxShips.Enabled = false;

            
            cbDirection.SelectedIndex = 0;
        }

        

        void gameController_PlayerWon(IPlayer obj)
        {
            MessageBox.Show(obj.getName() + " vyhrál!!!");
            Application.Exit();
        }


        void gameController_OnMessage1(string obj)
        {
            labelState.Text = obj;
        }

        private void pbGame_MouseClick(object sender, MouseEventArgs e)
        {
            if (gameController.type == 2 && !gameController.GetAttackPlayer().hasShipToPlace)
            {
                if (gameController.Turn(e.Location.CalculatePointToBoard(pbEnemyPlayerBoard.Size, gameController.SIZE)))
                {
                    MessageBox.Show(gameController.GetAttackPlayerName() + " vyhrál!!!");
                    Application.Exit();
                }
                //gameController.Turn(e.Location.CalculatePointToBoard(pbEnemyPlayerBoard.Size, gameController.SIZE));
                ReloadBoards();
                gameController.ComputerTurn();
            }

            if (gameController.type == 1 && !gameController.GetAttackPlayer().hasShipToPlace&& !gameController.GetDefendPlayer().hasShipToPlace)
            {
                if (gameController.Turn(e.Location.CalculatePointToBoard(pbEnemyPlayerBoard.Size, gameController.SIZE)))
                {
                    MessageBox.Show(gameController.GetAttackPlayerName() + " vyhrál!!!");
                    Application.Exit();
                }
                ReloadBoards();
                pbActualPlayerBoard.Image = gameController.GetImageHide(pbActualPlayerBoard.Size);
                gameController.SwapPlayers();
                setPlayersNames();
                
                MessageBox.Show("Hraje hráč: " + gameController.GetAttackPlayerName());
                

            }    
                
                //lEnemyPlayer.Text = gameController.getAttackPlayerName();
                //label1.Text = gameController.getDefendPlayerName();
                ReloadBoards();
                
        

        }

        private void ReloadBoards()
        {
            pbEnemyPlayerBoard.Image = gameController.GetImageEnemy(pbEnemyPlayerBoard.Size);
            pbActualPlayerBoard.Image = gameController.GetImageActualPlayer(pbActualPlayerBoard.Size);
        }

        private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewGame newGame = new NewGame(ref gameController);
            newGame.ShowDialog();
            this.gameController = newGame.getControl();
            pbActualPlayerBoard.Enabled = true;
            pbEnemyPlayerBoard.Enabled = true;


            buttonAddShip.Enabled = true;
            listBoxShips.Enabled = true;
            listBoxShips.SelectedIndex = 0;

            tableLayoutPanel1.Visible = true;
            tableLayoutPanel2.Visible = true;
            tableLayoutPanel3.Visible = true;
            tableLayoutPanel4.Visible = true;

            ReloadBoards();

            setPlayersNames();

            if (gameController.type == 2)
            {
                while (gameController.GetDefendPlayer().hasShipToPlace)
                {
                        gameController.GetDefendPlayer().PlaceShip(0, -1, -1, true);
                }   
            }
           
        }

        private void konecToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            Application.Exit();
        }

        private void numericRow_ValueChanged(object sender, EventArgs e)
        {

        }

        private void buttonAddShip_Click(object sender, EventArgs e)
        {
            placingShips();

        }

        void placingShips()
        {
            bool dir = (cbDirection.SelectedIndex < 1 ? true : false);


            if (gameController.GetAttackPlayer().hasShipToPlace)
            {
                if (gameController.GetAttackPlayer().PlaceShip(lbToShip(), Convert.ToInt16(numericR.Value), Convert.ToInt16(numericColumn.Value), !dir))
                {
                    listBoxShips.Items.RemoveAt(listBoxShips.SelectedIndex);
                    buttonAddShip.Enabled = false;

                    if (listBoxShips.Items.Count == 0 && !gameController.GetAttackPlayer().hasShipToPlace&& gameController.type == 1)
                    {
                        listBoxShips.Items.Add("Aircraft");
                        listBoxShips.Items.Add("Battleship");
                        listBoxShips.Items.Add("Submarine");
                        listBoxShips.Items.Add("Destroyer");
                        listBoxShips.Items.Add("Patrol");

                        listBoxShips.SelectedIndex = 0;
                        pbActualPlayerBoard.Image = gameController.GetImageHide(pbActualPlayerBoard.Size);
                        gameController.SwapPlayers();
                        MessageBox.Show("Lodě vkládá hráč:" + gameController.GetAttackPlayer().getName());

                    }

                    else if (!gameController.GetAttackPlayer().hasShipToPlace&& !gameController.GetDefendPlayer().hasShipToPlace&& gameController.type == 1)
                    {
                        buttonAddShip.Enabled = false;
                        listBoxShips.Enabled = false;
                        gameController.SwapPlayers();
                        setPlayersNames();
                        MessageBox.Show("Hraje hráč:" + gameController.GetAttackPlayer().getName());
                    }

                }

                ReloadBoards();
            }
           
        }

        void setPlayersNames()
        {
            labelEnemy.Text = gameController.GetDefendPlayerName();
            labelActual.Text = gameController.GetAttackPlayerName();
        }

        int lbToShip()
        {
            switch(listBoxShips.SelectedItem.ToString()){
                case "Aircraft":
                return 0;

                case "Battleship":
                return 1;

                case "Submarine":
                return 2;

                case "Destroyer":
                return 3;

                case "Patrol":
                return 4;
            }

            return -1;
        }

        private void listBoxShips_SelectedIndexChanged(object sender, EventArgs e)
        {
            buttonAddShip.Enabled = true;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {

            SaveFileDialog SaveFileDialog = new SaveFileDialog();
            SaveFileDialog.Title = "Uložit jako ...";
            SaveFileDialog.Filter = "Binární soubory (*.bin)|*.bin";
            SaveFileDialog.InitialDirectory = @"C:\";
            if (SaveFileDialog.ShowDialog() == DialogResult.OK)
            {
                FileStream stream = new FileStream(SaveFileDialog.FileName, FileMode.OpenOrCreate);

                BinaryWriter writer = new BinaryWriter(stream);

                writer.Write(gameController.type);

                writer.Write(gameController.GetAttackPlayer().getName());
                writer.Write(gameController.GetDefendPlayer().getName());

                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        writer.Write(gameController.GetAttackPlayer().getBoard()[i, j].Attacked);
                        writer.Write(gameController.GetAttackPlayer().getBoard().HasShip(i, j));

                    }
                }

                
                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        writer.Write(gameController.GetDefendPlayer().getBoard()[i, j].Attacked);
                        writer.Write(gameController.GetDefendPlayer().getBoard().HasShip(i, j));

                    }
                }

                writer.Close();
                stream.Close();
            }


           

        }

        /*private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog OpenFile = new OpenFileDialog();
            OpenFile.Title = "Otevřít ... ";
            OpenFile.Filter = "Binární soubor (*.bin)|*.bin";
            OpenFile.InitialDirectory = @"C:\";

            if (OpenFile.ShowDialog() == DialogResult.OK)
            {
                FileStream stream = new FileStream(OpenFile.FileName, FileMode.OpenOrCreate);

                BinaryReader reader = new BinaryReader(stream);

                int type = reader.Read();

                GameControl game = new GameControl();

                game.setPlayers(reader.ReadString(), reader.ReadString(), type);
                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {

                        game.getAttackPlayer().getBoard()[i, j].Attacked = reader.ReadBoolean();

                        if (reader.ReadBoolean())
                        {
                            game.getAttackPlayer().getBoard()[i, j] = new Ship(1);
                        }

                    }
                }


                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {

                        game.getDefendPlayer().getBoard()[i, j].Attacked = reader.ReadBoolean();

                        if (reader.ReadBoolean())
                        {
                            game.getDefendPlayer().getBoard()[i, j] = new Ship(1);
                        }

                    }
                }
                gameController = game;
                stream.Close();
                reader.Close();
            }

        }*/
    }
}
