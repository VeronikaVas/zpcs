﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SimpleCalculator
{
    /// <summary>
    /// Interakční logika pro MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void countButton_Click(object sender, RoutedEventArgs e)
        {
            string operation = operationComboBox.Text;
            double n1 = double.Parse(number1TextBox.Text);
            double n2 = double.Parse(number2TextBox.Text);
            double result = 0;

            switch (operation)
            {
                case "+":
                    result = n1 + n2;
                    break;
                case "-":
                    result = n1 - n2;
                    break;
                case "*":
                    result = n1 * n2;
                    break;
                case "/":
                    if (n2 != 0)
                        result = n1 / n2;
                    else
                        MessageBox.Show("Nulou nelze dělit");
                    break;
            }

            resultTextBlock.Text = result.ToString();


        }
    }
}
